#include "Server.h"
#include "Helper.h"
#include <fstream>

std::mutex mtx;

Server::Server()
{

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve()
{

	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(MY_PORT); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*) & sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << MY_PORT << std::endl;

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
	}
}


void Server::accept()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;

	// the function that handle the conversation with the client
	std::thread t1 (&Server::clientHandler, this, client_socket);
	t1.detach();
}

void Server::clientHandler(SOCKET clientSocket)
{
	std::string username,reciver, msg, newMsg, fileName;
	try
	{

		std::string login = Helper::getStringPartFromSocket(clientSocket, 1024);

		std::string len = login.substr(3,2);
		int Len = std::atoi(len.c_str());
		username = login.substr(5, Len);

		std::cout << "new user name is:" << username << std::endl;
		this->_users.insert(username);

		//send server's ServerUpdateMessage: (101 00000  00  numOfUsers namesOfUsers)
		//first msg is always not the same as the others
		Helper::send_update_message_to_client(clientSocket, "", "", getAllUsers());

		while (true)
		{
			std::string req = Helper::getStringPartFromSocket(clientSocket, 1024);
			//std::cout << "req is: " << req << std::endl;

			std::string req_Kind = req.substr(0, 3);
			int reqKind = std::atoi(req_Kind.c_str());

			if (reqKind == 204)
			{
				//seperating the reciver:
				std::string reciverLen = req.substr(3, 2);
				int reciver_Len = std::atoi(reciverLen.c_str());
				reciver = req.substr(5, reciver_Len);
				//std::cout << "reciver is: " << reciver << std::endl;

				//seperating the message:
				std::string LenMsg = req.substr(3 + 2 + reciver_Len, 5);
				int Len_Msg = std::atoi(LenMsg.c_str());
				msg = req.substr(3 + 2 + reciver_Len + 5, Len_Msg);
				//std::cout << "msg is: " << msg << std::endl;

				//creating the new msg:
				newMsg = "&MAGSH_MESSAGE&&Author&" + username + "&DATA&" + msg+"\n";

				//creating the file name:
				if (username < reciver)
					fileName = username + "&" + reciver + ".txt";
				else
					fileName = reciver + "&" + username + ".txt";

				//locking the option to use this file and/or socket 
				std::unique_lock<std::mutex> lck(mtx);

				CreateUsersFile(username, reciver, msg, newMsg, fileName); //writing the new content to the right file
				
				//putting together all the file content to send in the next call.
				std::ifstream file;
				std::string fileContent, fileLine;

				file.open(fileName);
				while (std::getline(file, fileLine))
					fileContent += fileLine;

				Helper::send_update_message_to_client(clientSocket, fileContent, reciver, getAllUsers());
				//releasing the locker
				lck.unlock();
			}
			//
		}
	}
	catch (const std::exception & e)
	{
		//removing the username from the connected users set.
		this->_users.erase(username);
		closesocket(clientSocket);
		std::cout <<e.what() << " - " << username << " left the chat." << std::endl;
	}


}

std::string Server::getAllUsers()
/*
	function creates string of all connected users at the momment with "&" between each other.
*/
{
	std::string sign = "&";
	std::string res = "";
	std::set<std::string>::iterator it;
	for (it = this->_users.begin(); it != this->_users.end(); it++)
	{
		res += *it;
		res += sign;
	}
	if(res.size() >= sign.size())
		//cutting down one last "&", it is unnecessary
		res = res.substr(0, res.size() - sign.size());
	return res;
}

void Server::CreateUsersFile(std::string writer, std::string reciver, std::string msg, std::string newMsg, std::string fileName)
/*
function writes into the file the new content of a convo between 2 users, when input is:
writer: username of the user that sent the msg.
reciver : username of the user that recived the msg.
msg: the string that is the content, without the protocol addition.
newMsg: the string that will be written in the file, with the protocol additions.
*/
{
	if (msg.empty() == false && reciver.empty() == false)
	//if there is a request for sending a msg to a user, opening the file and adding the new content
	{
		
		std::ofstream myfile;
		myfile.open(fileName, std::ios::app);
		if (myfile.is_open())
		{
			myfile << newMsg;
			myfile.close();
		}
	}
	else if (msg.empty()==true && reciver.empty()==false)
	//if there is a request for getting a content from a user but no msg to send, dont add nothing to the file
	{
		newMsg = "";
		reciver = reciver;
	}
	else if(msg.empty() && reciver.empty())
	//same here.
	{
		reciver = "";
		newMsg = "";
	}

	
}