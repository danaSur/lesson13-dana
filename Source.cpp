#pragma comment (lib, "ws2_32.lib")

#include "WSAInitializer.h"
#include "Server.h"
#include <iostream>
#include <exception>

int main()
{
	try
	{
		WSAInitializer wsaInit;
		Server myServer;

		myServer.serve();
	}
	catch (std::exception & e)
	{
		std::cout << "Error occured: " << e.what() << std::endl;
	}
	/*WSAInitializer wsaInit;
	Server myServer;
	myServer._users.insert("dana");
	myServer._users.insert("alon");
	std::cout << myServer.getAllUsers() << std::endl;
	myServer._users.insert("yotam");
	std::cout << myServer.getAllUsers() << std::endl;*/
	system("PAUSE");
	return 0;
}