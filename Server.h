#pragma once

#include <iostream>
#include <queue>
#include <set>
#include <vector>
#include <string.h>
#include <exception>
#include <string>
#include <thread>
#include <mutex>

#include <WinSock2.h>
#include <Windows.h>

#define MY_PORT 8826

class Server
{
	//private:
	std::queue<std::string> _msgs;
	std::set<std::string> _users;

	std::string getAllUsers();
	///////
	void accept();
	void clientHandler(SOCKET clientSocket);


	SOCKET _serverSocket;

	//create txt file:
public:
	Server();
	~Server();
	void serve();
	void CreateUsersFile(std::string writer, std::string reciver, std::string msg, std::string newMsg, std::string fileName);

};
